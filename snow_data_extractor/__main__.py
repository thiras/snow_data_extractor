#!/usr/bin/env python3
import csv
import operator
import os
import re
from collections import OrderedDict
from datetime import datetime as dt

import click
import h5py

from .latlon2pix import ease2grid

DIR = os.getcwd()


# Autocompletion
def get_env_vars(ctx, args, incomplete):
    return [k for k in os.environ.keys() if incomplete in k]


@click.group()
def cli():
    pass


@click.command()
@click.argument("latitude")
@click.argument("longitude")
@click.option(
    "--hemisphere",
    default="northern",
    help="Specify the hemisphere. Default is northern.",
)
def convert2ease(latitude: float, longitude: float, hemisphere: str):
    """
    This command converts EPSG:4326 Latitude and Longitude to EASE-Grid 2.0

    Arguments:

        LATITUDE: EPSG:4326 Latitude coordinate of the point.

        LONGITUDE: EPSG:4326 Longitude coordinate of the point.

    """
    hemisphere = hemisphere.lower()
    latitude = float(latitude)
    longitude = float(longitude)

    if hemisphere == "northern":
        click.echo(ease2grid(8, latitude, longitude, 0, 0))
    elif hemisphere == "southern":
        click.echo(ease2grid(9, latitude, longitude, 0, 0))


cli.add_command(convert2ease)


@click.command()
@click.argument("data_path", type=click.Path(exists=True))
@click.argument("output_name")
@click.argument("x_grid")
@click.argument("y_grid")
@click.option(
    "--sphere", default="northern", help="Specify the hemisphere. Default is northern."
)
def read_data(data_path: str, output_name: str, x_grid: int, y_grid: int, sphere: str):
    """
    This command reads the data in bulk for given grid.

    """
    click.echo("Reading data. This may take a while...")

    data_path = os.path.join(DIR, data_path)
    sphere = sphere.lower().capitalize()
    x_grid = int(x_grid)
    y_grid = int(y_grid)

    result = dict()

    for filename in os.listdir(data_path):
        f = h5py.File(os.path.join(data_path, filename), "r")
        date = dt.strptime(re.search("([0-9]{8})", filename).group(0), "%Y%m%d").date()
        dset = f["HDFEOS"]["GRIDS"]["{} Hemisphere".format(sphere)]["Data Fields"][
            "SWE_{}Daily".format(sphere)
        ]

        result[date] = dset[x_grid, y_grid]

    ordered_result = OrderedDict(sorted(result.items(), key=operator.itemgetter(0)))

    try:
        click.echo("Writing data to csv")
        with open(output_name, "w") as f:
            writer = csv.writer(f)
            for key, value in ordered_result.items():
                writer.writerow([key, value])
        click.echo("Done")
    except IOError:
        click.echo("Unable to write to disk")


cli.add_command(read_data)


if __name__ == "__main__":
    cli()
