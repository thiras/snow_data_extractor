import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="snow-data-extractor",
    version="0.1.0",
    author="Ant Somers",
    author_email="antsomers@gmail.com",
    description="A small script to extract bulk data from NSIDC snow data.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/thiras/snow_data_extractor",
    packages=setuptools.find_packages(),
    install_requires=["click", "h5py"],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: GIS",
        "Topic :: Scientific/Engineering :: Hydrology",
        "Topic :: Scientific/Engineering :: Information Analysis",
    ],
    entry_points={"console_scripts": ["sde = snow_data_extractor.__main__:cli"]},
    python_requires=">=3.6",
    zip_safe=False,
)
